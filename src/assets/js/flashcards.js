window.onload = (event) => {

    const init_flashcards = function () {
        class singleCard {
            constructor(card) {
                card.setAttribute('flip', 'no');
                card.addEventListener('click', (e) => {
                    let elt = e.currentTarget;
                    if (elt.getAttribute("flip") == "no") {
                        elt.setAttribute("flip", "yes");
                        playSound('flip_up');
                    } else {
                        elt.setAttribute("flip", "no");
                        playSound('flip_down');
                    }
                });
            }
        }

        class multiCard {
            constructor(elt) {
                // Image de fin 
                let bravo_img;
                if (typeof FC_BRAVO_IMG === "undefined") {
                    bravo_img = "audio/";
                } else {
                    bravo_img = FC_BRAVO_IMG;
                }
                // Initialisation
                this.element = elt;
                let cardNodes = elt.querySelectorAll('.card');
                this.cards = [];
                cardNodes.forEach((c) => {
                    c.setAttribute("flip", "no");
                    this.cards.push(c);
                });
                this.numberOfCard = this.cards.length;
                this.state = "ask";
                for (let i = 1; i < this.cards.length; i++) {
                    this.cards[i].classList.add('hidden');
                    this.cards[i].style.cursor = "grab";
                }

                // Ajout de la carte de fin
                this.divEnd = document.createElement('div');
                this.divEnd.classList.add('card', 'hidden', 'final');
                this.divEnd.style.background = 'url("' + bravo_img + '")';
                this.divEnd.style['background-size'] = 'cover';
                this.element.appendChild(this.divEnd);

                // Ajout de la zone inférieure
                this.divHint = document.createElement('div');
                this.divHint.classList.add('hint');
                this.divHint.innerHTML = "<p> Essayez de vous rappellez puis vérifiez </p>";
                this.divHint.innerHTML += "<p class=\"text-muted\"> (Nombre de cartes restantes : " + this.numberOfCard + ")</p>";
                elt.appendChild(this.divHint);

                this.divVerify = document.createElement('div');
                this.divVerify.classList.add('verify');
                this.divVerify.classList.add('hidden');
                this.divVerify.innerHTML = "<p>Vous en souveniez vous ?  </p>";

                let p = document.createElement('p');

                this.retryButton = document.createElement('button');
                this.retryButton.classList.add('btn', 'btn-warning', 'mx-2', 'mb-2');
                this.retryButton.innerHTML = "Non, on réessaye.";
                p.appendChild(this.retryButton);

                this.validateButton = document.createElement('button');
                this.validateButton.classList.add('btn', 'btn-success', 'mx-2', 'mb-2');
                this.validateButton.innerHTML = "Oui, on continue !";
                p.appendChild(this.validateButton);

                this.divVerify.appendChild(p);

                elt.appendChild(this.divVerify);

                this.cards.forEach(card => {
                    card.addEventListener('click', (e) => {
                        this.clicked();
                    });
                });

                this.retryButton.addEventListener('click', (e) => {
                    playSound('button_down');
                    this.retry();
                });

                this.validateButton.addEventListener('click', (e) => {
                    playSound('button_up');
                    this.validate();
                });

            }

            clicked() {
                let card = this.cards[0];
                if (card.getAttribute("flip") == "no") {
                    card.setAttribute("flip", "yes");
                    playSound('flip_up');
                } else {
                    card.setAttribute("flip", "no");
                    playSound('flip_down');
                }

                if (this.state == "ask") {
                    this.divHint.classList.add('hidden');
                    this.divVerify.classList.remove('hidden');
                    this.state = "verify";
                }
            }

            validate() {
                this.divHint.classList.remove('hidden');
                this.divVerify.classList.add('hidden');
                let card = this.cards[0];
                let currentStyle = getComputedStyle(card);
                card.style['margin-left'] = currentStyle['margin-left'];
                card.style['z-index'] = 10;
                if (this.cards.length > 1) {
                    this.cards[1].classList.remove('hidden');
                    if (this.cards[1] != this.element.querySelector('.card')) {
                        this.cards[1].style['margin-top'] = "-" + currentStyle.height;
                    } else {
                        this.cards[0].style['margin-top'] = "-" + currentStyle.height;
                    }
                } else {
                    this.divHint.classList.add('hidden');
                    this.divVerify.classList.add('hidden');
                    this.divEnd.classList.remove('hidden');
                    this.divEnd.style['margin-top'] = "-" + currentStyle.height;
                }
                //card.style.transition = 'margin-left 0.5s ease-in-out';
                //card.style['margin-left'] = '20px';
                card.classList.add('vanishing-animated');
                document.querySelector('html').style['overflow-x']='hidden';
                setTimeout(function (that) {
                    that.cards.shift().remove();
                    document.querySelector('html').style['overflow-x']='auto';
                    if (that.cards.length > 0) {
                        that.cards[0].style['margin-top'] = "0px";
                        that.divHint.innerHTML = "<p> Essayez de vous rappellez puis vérifiez </p>";
                        that.divHint.innerHTML += "<p class=\"text-muted\"> (Nombre de cartes restantes : " + that.cards.length + ")</p>";
                        that.state = "ask";
                    } else {
                        that.divEnd.style['margin-top'] = "0px";
                        playSound('ding');
                    }
                }, 750, this);
            }

            retry() {
                this.divHint.classList.remove('hidden');
                this.divVerify.classList.add('hidden');
                this.cards[0].setAttribute("flip", "no");

                if (this.cards.length > 1) {
                    let card = this.cards[0];
                    let next = this.cards[1];
                    let currentStyle = getComputedStyle(card);
                    let final = currentStyle['margin-left'];
                    card.style['margin-left'] = final;
                    card.style.transition = 'margin-left 0.3s ease-in-out';
                    card.style['z-index'] = '10';
                    if (next != this.element.querySelector('.card')) {
                        next.style['margin-top'] = "-" + currentStyle.height;
                    } else {
                        card.style['margin-top'] = "-" + currentStyle.height;
                    }
                    next.classList.remove('hidden');
                    card.style['margin-left'] = '-600px';
                    setTimeout(function (card, netx, final, that) {
                        card.style['z-index'] = '-10';
                        card.style['margin-left'] = final;
                        setTimeout(function (card, next, that) {
                            card.classList.add('hidden');
                            card.style['z-index'] = 'auto';
                            card.style.margin = '0 auto';
                            card.style.transition = 'transform 0.4s ease 0s';
                            that.cards.push(that.cards.shift());
                            next.style['margin-top'] = '0';

                        }, 350, card, next, that);
                    }, 350, card, next, final, this);


                }
                this.state = "ask";
            }
        }

        const SOUNDS_TO_LOAD = [
            ["button_down", 1],
            ["button_up", 1],
            ["flip_down", 1],
            ["flip_up", 1],
            ["ding", 1],
        ];

        let SOUNDS = {};
        function loadSounds() {
            let sound_dir;
            if (typeof FC_SOUND_DIRECTORY === "undefined") {
                sound_dir = "audio/";
            } else {
                sound_dir = FC_SOUND_DIRECTORY;
            }
            SOUNDS_TO_LOAD.forEach(function (config) {
                var name = config[0];
                var vol = config[1];
                SOUNDS[name] = new Howl({
                    src: [sound_dir + name + ".mp3"],
                    volume: vol
                });
            });
        }

        playSound = function (name) {
            SOUNDS[name].play();
        };

        function initialisation() {
            loadSounds();

            var cards = document.querySelectorAll('.cardBox .card');
            cards.forEach(card => {
                new singleCard(card);
            });

            var multiCards = document.querySelectorAll('.multiCardBox');
            multiCards.forEach(multi => {
                new multiCard(multi);
            });

        }

        initialisation();
    }();
};